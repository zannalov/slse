# Salesforce Lightning Search Engine

Create a custom search engine within your browser:
* [Chrome](https://support.google.com/customsearch/answer/4513882?hl=en)
* [Firefox](https://support.mozilla.org/en-US/kb/add-or-remove-search-engine-firefox#w_add-a-search-engine)

Set the URL to be: `https://slse.herokuapp.com/?lightningForceOrg={org}&search=%s`

NOTE: Replace `{org}` with your force.com org name. For example if your browser would normally say `https://myOrg.lightning.force.com/...` then you would use `https://slse.herokuapp.com/?lightningForceOrg=myOrg&search=%s`

Now type your keyword into your browser search bar, press space, and enter your search string. I use the org name as the search keyword, so in the above example typing "myOrg 123" would search the entire org for "123".
