const URL = require('url').URL
const http = require('http')
const port = process.env.PORT || 5000

function handler (req, res) {
  let url = new URL('https://' + req.headers.host + '/' + req.url.replace(/^\//, ''))
  const lightningForceOrg = url.searchParams.get('lightningForceOrg')
  const search = url.searchParams.get('search')

  let redirect = 'https://gitlab.com/zannalov/slse/blob/master/README.markdown'

  if (lightningForceOrg && search) {
    const hashObject = {
      componentDef: 'forceSearch:search',
      attributes: {
        term: search,
      }
    }
    const jsonEncoded = JSON.stringify(hashObject)
    const base64Encoded = Buffer.from(jsonEncoded).toString('base64')
    const urlEncoded = encodeURIComponent(base64Encoded)
    redirect = 'https://' + encodeURIComponent(lightningForceOrg) + '.lightning.force.com/one/one.app#' + urlEncoded
  }

  res.setHeader('Location', redirect)
  res.writeHead(302, 'Found')
  res.end()
}

const server = http.Server(handler)
server.listen(port, () => {
  console.log('Listening on port %d', port)
})
